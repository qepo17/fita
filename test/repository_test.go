package test_test

import (
	"fita/business"
	"fita/repository"
	"log"
	"testing"
	"time"

	"github.com/ory/dockertest/v3"
	"github.com/ory/dockertest/v3/docker"
	"github.com/stretchr/testify/assert"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var appointmentUsecase business.AppointmentUsecase

func init() {
	var err error
	var db *gorm.DB

	pool, err := dockertest.NewPool("")
	pool.MaxWait = time.Minute * 2
	if err != nil {
		log.Fatalf("Could not connect to docker: %s", err)
	}

	opts := dockertest.RunOptions{
		Repository: "mysql/mysql-server",
		Tag:        "5.7.25",
		Env: []string{
			"MYSQL_ROOT_PASSWORD=root",
			"MYSQL_USER=root",
			"MYSQL_PASSWORD=root",
			"MYSQL_DATABASE=fita",
		},
		ExposedPorts: []string{"3333"},
		PortBindings: map[docker.Port][]docker.PortBinding{
			"3333": {
				{HostIP: "127.0.0.1", HostPort: "3333"},
			},
		},
	}

	resource, err := pool.RunWithOptions(&opts)
	if err != nil {
		log.Fatalf("Could not start resource: %s", err)
	}

	if err = pool.Retry(func() error {
		var err error
		db, err := gorm.Open(mysql.Open("root:root@tcp(127.0.0.1:3333)/fita?charset=utf8mb4"), &gorm.Config{})
		if err != nil {
			return err
		}
		return db.Error
	}); err != nil {
		log.Fatalf("Could not connect to docker: %s", err)
	}

	initDB(db)
	initData(db)
	initUsecase(db)

	// When you're done, kill and remove the container
	if err = pool.Purge(resource); err != nil {
		log.Fatalf("Could not purge resource: %s", err)
	}
}

func initDB(db *gorm.DB) {
	db.Exec("CREATE DATABASE fita;")
	db.AutoMigrate(&business.User{})
	db.AutoMigrate(&business.Availability{})
	db.AutoMigrate(&business.Appointment{})
}

func initData(db *gorm.DB) {
	// Setup

	user := []business.User{
		{
			Name:      "Test User",
			Email:     "test@gmail.com",
			Password:  "123456",
			Type:      "admin",
			Timezone:  "Asia/Jakarta",
			CreatedAt: business.GenerateUTCTimestamp(),
			CreatedBy: 1,
		},
		{
			Name:      "Test Doctor",
			Email:     "test@gmail.com",
			Password:  "123456",
			Type:      "doctor",
			Timezone:  "UTC",
			CreatedAt: business.GenerateUTCTimestamp(),
			CreatedBy: 1,
		},
		{
			Name:      "Test Admin",
			Email:     "test@gmail.com",
			Password:  "123456",
			Type:      "user",
			Timezone:  "Asia/Jakarta",
			CreatedAt: business.GenerateUTCTimestamp(),
			CreatedBy: 1,
		},
	}

	db.Create(&user)

	availability := business.Availability{
		UserID:    2,
		Day:       "Monday",
		StartTime: "10:00",
		EndTime:   "15:00",
		CreatedAt: business.GenerateUTCTimestamp(),
		CreatedBy: 1,
	}

	db.Create(&availability)
}
func initUsecase(db *gorm.DB) {
	userRepo := repository.NewUser(db)
	availabilityRepo := repository.NewAvailability(db)
	appointmentRepo := repository.NewAppointment(db)

	userUsecase := business.NewUser(userRepo)
	availabilityUsecase := business.NewAvailability(availabilityRepo)
	appointmentUsecase = business.NewAppointment(appointmentRepo, availabilityUsecase, userUsecase)
}

func Test_CreateAppointmentSuccess(t *testing.T) {
	// Setup
	appointment := business.AppointmentCreateReq{
		DoctorID:  2,
		UserID:    1,
		StartTime: "10:00",
		EndTime:   "11:00",
		Day:       "Monday",
	}

	// Execute
	err := appointmentUsecase.CreateAppointment(&appointment)

	// Assert
	assert.Equal(t, nil, err)
}
