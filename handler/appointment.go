package handler

import (
	"fita/business"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type AppointmentHandler interface {
	CreateAppointment(c *gin.Context)
	GetApprovedAppointmentsByUserID(c *gin.Context)
	GetRescheduleAppointmentsByUserID(c *gin.Context)
}

type appointmentHandler struct {
	appointmentUsecase business.AppointmentUsecase
}

func NewAppointmentHandler(appointmentUsecase business.AppointmentUsecase) AppointmentHandler {
	return &appointmentHandler{
		appointmentUsecase: appointmentUsecase,
	}
}

func (h *appointmentHandler) CreateAppointment(c *gin.Context) {
	var appointment business.AppointmentCreateReq

	if err := c.ShouldBindJSON(&appointment); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	err := h.appointmentUsecase.CreateAppointment(&appointment)
	if err != nil {
		if err == business.ErrAppointmentAvailabilyNotValid {
			BadRequestResponse(c, err)
		}
		InternalServerResponse(c, err)
		return
	}

	SuccessResponse(c, nil)
}

func (h *appointmentHandler) GetApprovedAppointmentsByUserID(c *gin.Context) {
	userIDStr := c.Param("id")
	userID, err := strconv.Atoi(userIDStr)
	if err != nil {
		BadRequestResponse(c, err)
		return
	}

	appointments, err := h.appointmentUsecase.GetApprovedAppointmentsByUserID(int32(userID))
	if err != nil {
		InternalServerResponse(c, err)
		return
	}

	SuccessResponse(c, appointments)
}

func (h *appointmentHandler) GetRescheduleAppointmentsByUserID(c *gin.Context) {
	userIDStr := c.Param("id")
	userID, err := strconv.Atoi(userIDStr)
	if err != nil {
		BadRequestResponse(c, err)
		return
	}

	appointments, err := h.appointmentUsecase.GetRescheduleAppointmentsByUserID(int32(userID))
	if err != nil {
		InternalServerResponse(c, err)
		return
	}

	SuccessResponse(c, appointments)
}

func (h *appointmentHandler) UpdateAppointmentStatusByID(c *gin.Context) {
	var appointment business.AppointmentUpdateStatusByIDReq
	if err := c.ShouldBindJSON(&appointment); err != nil {
		BadRequestResponse(c, err)
		return
	}

	err := h.appointmentUsecase.UpdateAppointmentStatusByID(&appointment)
	if err != nil {
		InternalServerResponse(c, err)
		return
	}

	SuccessResponse(c, nil)
}
