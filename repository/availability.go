package repository

import (
	"fita/business"

	"gorm.io/gorm"
)

type availabilityRepository struct {
	db *gorm.DB
}

func NewAvailability(db *gorm.DB) business.AvailabilityRepository {
	return &availabilityRepository{
		db: db,
	}
}

func (r *availabilityRepository) GetByDayAndUser(availability *business.AvailabilityByDayAndUserReq) (business.AvailabilityByDayAndUserResp, error) {
	result := business.AvailabilityByDayAndUserResp{}

	err := r.db.Select("start_time, end_time, users.timezone").Where("user_id = ? AND day = ?", availability.UserID, availability.Day).Joins("join users ON users.id = availabilities.created_by").First(&result).Error
	if err != nil {
		if err == gorm.ErrRecordNotFound {
			return result, nil
		}
		return result, err
	}

	return result, err
}
