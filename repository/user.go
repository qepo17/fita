package repository

import (
	"fita/business"

	"gorm.io/gorm"
)

type userRepository struct {
	db *gorm.DB
}

func NewUser(db *gorm.DB) business.UserRepository {
	return &userRepository{
		db: db,
	}
}

func (r *userRepository) GetByID(id int32) (*business.User, error) {
	user := business.User{}
	if err := r.db.Where("id = ?", id).First(&user).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return &user, nil
		}
		return &user, err
	}
	return &user, nil
}
