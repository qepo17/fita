package repository

import (
	"fita/business"

	"gorm.io/gorm"
)

type appointmentRepository struct {
	db *gorm.DB
}

func NewAppointment(db *gorm.DB) business.AppointmentRepository {
	return &appointmentRepository{
		db: db,
	}
}

func (r *appointmentRepository) CreateAppointment(appointment *business.Appointment) error {
	return r.db.Table("appointments").Create(appointment).Error
}

func (r *appointmentRepository) GetAppointmentsByUserIDAndStatus(userID int32, status string) ([]business.Appointment, error) {
	appointments := []business.Appointment{}
	if err := r.db.Table("appointments").Where("created_by = ? AND status = ?", userID, status).Scan(&appointments).Error; err != nil {
		return appointments, err
	}

	return appointments, nil
}

func (r *appointmentRepository) UpdateAppointmentStatusByID(req *business.AppointmentUpdateStatusByIDReq) error {
	return r.db.Table("appointments").Where("id = ?", req.ID).Update("status", req.Status).Error
}
