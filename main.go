package main

import (
	"fita/business"
	"fita/config"
	"fita/handler"
	"fita/repository"

	"github.com/gin-gonic/gin"
)

func main() {
	cfg := config.InitConfig()
	db := config.InitDB(cfg)

	// User
	userRepo := repository.NewUser(db)
	userUsecase := business.NewUser(userRepo)

	// Availability
	availabilityRepo := repository.NewAvailability(db)
	availabilityUsecase := business.NewAvailability(availabilityRepo)

	// Appointment
	appointmentRepo := repository.NewAppointment(db)
	appointmentUsecase := business.NewAppointment(appointmentRepo, availabilityUsecase, userUsecase)
	appointmentHandler := handler.NewAppointmentHandler(appointmentUsecase)

	router := gin.Default()
	routes(router, appointmentHandler)
}

func routes(r *gin.Engine, appointment handler.AppointmentHandler) {
	// Appointment
	r.POST("/appointments", appointment.CreateAppointment)
	r.GET("/appointments", appointment.GetApprovedAppointmentsByUserID)
	r.GET("/appointments/reschedules", appointment.GetRescheduleAppointmentsByUserID)
}
