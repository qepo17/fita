package business

type AvailabilityRepository interface {
	GetByDayAndUser(availability *AvailabilityByDayAndUserReq) (AvailabilityByDayAndUserResp, error)
}

type AvailabilityUsecase interface {
	GetByDayAndUser(availability *AvailabilityByDayAndUserReq) (AvailabilityByDayAndUserResp, error)
}

type availabilityUsecase struct {
	repo AvailabilityRepository
}

func NewAvailability(repo AvailabilityRepository) AvailabilityUsecase {
	return &availabilityUsecase{
		repo: repo,
	}
}

type Availability struct {
	ID        int32  `json:"id" gorm:"primaryKey"`
	UserID    int32  `json:"user_id"`
	Day       string `json:"day"`
	StartTime string `json:"start_time"`
	EndTime   string `json:"end_time"`
	CreatedAt int32  `json:"created_at"`
	CreatedBy int32  `json:"created_by"`
	UpdatedAt int32  `json:"updated_at"`
	UpdatedBy int32  `json:"updated_by"`
}

type AvailabilityByDayAndUserReq struct {
	UserID int32  `json:"user_id"`
	Day    string `json:"day"`
}

type AvailabilityByDayAndUserResp struct {
	StartTime string `json:"start_time"`
	EndTime   string `json:"end_time"`
	Timezone  string `json:"timezone"`
}

func (u *availabilityUsecase) GetByDayAndUser(availability *AvailabilityByDayAndUserReq) (AvailabilityByDayAndUserResp, error) {
	return u.repo.GetByDayAndUser(availability)
}
