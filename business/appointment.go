package business

type AppointmentRepository interface {
	CreateAppointment(req *Appointment) error
	GetAppointmentsByUserIDAndStatus(userID int32, status string) ([]Appointment, error)
	UpdateAppointmentStatusByID(req *AppointmentUpdateStatusByIDReq) error
}

type AppointmentUsecase interface {
	CreateAppointment(req *AppointmentCreateReq) error
	GetApprovedAppointmentsByUserID(userID int32) ([]Appointment, error)
	GetRescheduleAppointmentsByUserID(userID int32) ([]Appointment, error)
	UpdateAppointmentStatusByID(req *AppointmentUpdateStatusByIDReq) error
}

type appointmentUsecase struct {
	repo         AppointmentRepository
	availability AvailabilityUsecase
	user         UserUsecase
}

func NewAppointment(repo AppointmentRepository, availability AvailabilityUsecase, user UserUsecase) AppointmentUsecase {
	return &appointmentUsecase{
		repo:         repo,
		availability: availability,
		user:         user,
	}
}

const (
	AppointmentPendingStatus    = "pending"
	AppointmentRescheduleStatus = "reschedule"
	AppointmentDoneStatus       = "done"
	AppointmentRejectedStatus   = "rejected"
	AppointmentApprovedStatus   = "approved"
)

type Appointment struct {
	ID        int32  `json:"id" gorm:"primaryKey"`
	DoctorID  int32  `json:"doctor_id"`
	Day       string `json:"day"`
	StartTime string `json:"start_time"`
	EndTime   string `json:"end_time"`
	Status    string `json:"status"`
	CreatedAt int32  `json:"created_at"`
	CreatedBy int32  `json:"created_by"`
	UpdatedAt int32  `json:"updated_at"`
	UpdatedBy int32  `json:"updated_by"`
}

type AppointmentCreateReq struct {
	DoctorID  int32  `json:"doctor_id"`
	UserID    int32  `json:"user_id"`
	Day       string `json:"day"`
	StartTime string `json:"start_time"`
	EndTime   string `json:"end_time"`
}

type AppointmentUpdateStatusByIDReq struct {
	ID     int32  `json:"id"`
	Status string `json:"status"`
}

func (u *appointmentUsecase) CreateAppointment(req *AppointmentCreateReq) error {
	availability, err := u.availability.GetByDayAndUser(&AvailabilityByDayAndUserReq{
		UserID: req.UserID,
		Day:    req.Day,
	})
	if err != nil {
		return err
	}
	if availability == (AvailabilityByDayAndUserResp{}) {
		return ErrAppointmentAvailabilyNotValid
	}

	appointmentStartTime := ConvertSpecificTimeToUTC(req.StartTime)
	appointmentEndTime := ConvertSpecificTimeToUTC(req.EndTime)

	if availability.StartTime < appointmentStartTime || availability.EndTime > appointmentEndTime {
		return ErrAppointmentAvailabilyNotValid
	}

	// Convert local time to UTC to store in database
	// UserID could be stored from token
	// Sorry my time limited, cannot implement it here
	// Because I need to implement the auth and user usecase
	appointmentDB := Appointment{
		DoctorID:  req.DoctorID,
		Day:       req.Day,
		StartTime: appointmentStartTime,
		EndTime:   appointmentEndTime,
		Status:    AppointmentPendingStatus,
		CreatedAt: GenerateUTCTimestamp(),
		CreatedBy: req.UserID,
	}

	return u.repo.CreateAppointment(&appointmentDB)
}

func (u *appointmentUsecase) GetApprovedAppointmentsByUserID(userID int32) ([]Appointment, error) {
	appointments, err := u.repo.GetAppointmentsByUserIDAndStatus(userID, AppointmentApprovedStatus)
	if err != nil {
		return nil, err
	}

	// Timezone & UserID could be stored from token
	// Sorry my time limited, cannot implement it here
	// Because I need to implement the auth and user usecase
	user, err := u.user.GetByID(userID)
	if err != nil {
		return nil, err
	}

	for i := range appointments {
		appointments[i].StartTime = ConvertUTCtoSpecificTime(appointments[i].StartTime, user.Timezone)
		appointments[i].EndTime = ConvertUTCtoSpecificTime(appointments[i].EndTime, user.Timezone)
	}

	return appointments, nil
}

func (u *appointmentUsecase) GetRescheduleAppointmentsByUserID(userID int32) ([]Appointment, error) {
	appointments, err := u.repo.GetAppointmentsByUserIDAndStatus(userID, AppointmentRescheduleStatus)
	if err != nil {
		return nil, err
	}

	// Timezone & UserID could be stored from token
	// Sorry my time limited, cannot implement it here
	// Because I need to implement the auth and user usecase
	user, err := u.user.GetByID(userID)
	if err != nil {
		return nil, err
	}

	for i := range appointments {
		appointments[i].StartTime = ConvertUTCtoSpecificTime(appointments[i].StartTime, user.Timezone)
		appointments[i].EndTime = ConvertUTCtoSpecificTime(appointments[i].EndTime, user.Timezone)
	}

	return appointments, nil
}

func (u *appointmentUsecase) UpdateAppointmentStatusByID(req *AppointmentUpdateStatusByIDReq) error {
	// UserID could be stored from token
	// CreatedBy here should be from the UserID
	return u.repo.UpdateAppointmentStatusByID(req)
}
