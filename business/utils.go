package business

import (
	"errors"
	"time"
)

var (
	// Appointment
	ErrAppointmentAvailabilyNotValid = errors.New("appointment time is not in availability range")
)

func GenerateUTCTimestamp() int32 {
	return int32(time.Now().UTC().Unix())
}

func ConvertSpecificTimeToUTC(userTime string) string {
	ut, err := time.LoadLocation("UTC")
	if err != nil {
		return ""
	}

	t, err := time.ParseInLocation("15:04", userTime, ut)
	if err != nil {
		return ""
	}

	return t.String()
}

func ConvertUTCtoSpecificTime(utcTime string, userTimezone string) string {
	ut, err := time.LoadLocation(userTimezone)
	if err != nil {
		return ""
	}

	t, err := time.ParseInLocation("15:04", utcTime, ut)
	if err != nil {
		return ""
	}

	return t.String()
}
