package business

type UserRepository interface {
	GetByID(id int32) (*User, error)
}

type UserUsecase interface {
	GetByID(id int32) (*User, error)
}

type userUsecase struct {
	userRepo UserRepository
}

func NewUser(userRepo UserRepository) UserUsecase {
	return &userUsecase{
		userRepo: userRepo,
	}
}

type User struct {
	ID        int32 `gorm:"primaryKey"`
	Name      string
	Email     string
	Password  string
	Type      string
	Timezone  string
	CreatedAt int32
	CreatedBy int32
	UpdatedAt int32
	UpdatedBy int32
}

func (u *userUsecase) GetByID(id int32) (*User, error) {
	user, err := u.userRepo.GetByID(id)
	if err != nil {
		return nil, err
	}
	return user, nil
}
