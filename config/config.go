package config

import (
	"github.com/kelseyhightower/envconfig"
)

type Config struct {
	DBHost string `envconfig:"DB_HOST" default:"localhost"`
	DBPort string `envconfig:"DB_PORT" default:"3306"`
	DBUser string `envconfig:"DB_USER" default:"root"`
	DBPass string `envconfig:"DB_PASS" default:"password"`
	DBName string `envconfig:"DB_NAME" default:"fita"`
}

func InitConfig() *Config {
	var cfg Config
	envconfig.Process("", &cfg)
	return &cfg
}
