# BE - Appointment

## Explanation

Firstly, sorry I cannot finish the task as the asked requirement, to be honest, I have very limited time to work on it. Approximately, it is about 7hours

I cannot finish the unit test, even cannot test if the apps is work or not. I am sorry for this

But I will explain the design I made about this

### Database

[ERD](./erd.png)

- We need to store Timezone in user's data to know the local time of the user
- We need to store time (Start Time & End Time) in appointments & availabilities as UTC time
  - When we get the data from those table, we can convert it later to user's timezone
- CreatedAt, UpdatedAt, DeletedAt should be stored as UNIX timestamp (in UTC)

### Code

- For the test, I want use dockertest so it counts as Integration Test
- Unit test start from the usecase
- Business logic cannot depend on another layer, but another layer should depend on them

### How to run migration

```
UP
migrate -path migration -database "mysql://root:root@tcp(localhost:3306)/fita?" -verbose up

DOWN
migrate -path migration -database "mysql://root:root@tcp(localhost:3306)/fita?" -verbose up
```
