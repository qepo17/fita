CREATE TABLE `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `type` ENUM('admin','doctor','user') NOT NULL DEFAULT 'user',
  `timezone` varchar(255) NOT NULL DEFAULT 'Asia/Jakarta',
  `created_at` bigint(20) NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `updated_at` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `deleted_at` bigint(20) DEFAULT NULL,
  `deleted_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `availabilities` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `day` ENUM('sunday','monday','tuesday','wednesday','thursday','friday','saturday') NOT NULL,
  `start_time` char(4) NOT NULL COMMENT 'Default UTC Time',
  `end_time` char(4) NOT NULL COMMENT 'Default UTC Time',
  `created_at` bigint(20) NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `updated_at` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `appointments` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `doctor_id` bigint(20) NOT NULL,
  `day` ENUM('sunday','monday','tuesday','wednesday','thursday','friday','saturday') NOT NULL,
  `start_time` char(4) NOT NULL COMMENT 'Default UTC Time',
  `end_time` char(4) NOT NULL COMMENT 'Default UTC Time',
  `status` ENUM('pending', 'approved', 'reschedule', 'rejected', 'done') NOT NULL DEFAULT 'pending',
  `created_at` bigint(20) NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `updated_at` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE INDEX IDX_user_id_day
ON availabilities (`user_id`,`day`);

CREATE INDEX IDX_doctor_id
ON appointments (`doctor_id`);

CREATE INDEX IDX_created_by
ON appointments (`created_by`);

CREATE INDEX IDX_status
ON appointments (`status`);